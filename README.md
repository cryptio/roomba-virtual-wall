# Roomba Virtual Wall

Homemade Roomba Virtual Wall

## Requirements
In order to create a Roomba Virtual Wall, you must have a...
- 38KHz IR LED
- suitable resistor for the LED (I went with 220Ω, but anything around this value should be sufficient)
- jumper cable

## Techical overview
As explained by Justin from [misc.ws](https://misc.ws/2014/02/27/diy-virtual-wall-for-roomba/):

> It turns out that the virtual wall isn’t too complex.  Basically, it sends out a repeating pattern of ON/OFF infrared signals, each one a millisecond long.  These signals ride on a 38kHz carrier frequency, just like most infrared remote controls, meaning that an ON signal is actually being pulsed ON/OFF about 38,000 times per second.

## Wiring
The wiring is shown below:

![Virtual Wall diagram](img/virtualwall_bb.png)

For the Nano, the negative leg of your IR LED must be wired to the D3 pin. This is hardcoded into the IRremote library being used, and may vary depending on which Arduino board you have. The positive leg (anode) must be connected to the 5V pin. The virtual wall will not work if you wire the cathode to ground.

In its current setup, this acts only in virtual wall mode, meaning it is not capable of creating a 4-foot halo like what iRobot's Dual Mode Virtual Wall Barrier does. The LED should be placed in a horizontal position so that the top of it is able to create a line of infrared light that acts as a wall. One could ideally replace the standard IR LED being used with something that created a dome of infrared light, or simply attach an IR LED to a servo and have it constantly rotate in a circular motion. The LEDs I used are capable of emitting an IR signal up to 12-13 meters, which provides more coverage than the advertised range of 10 feet for the product sold directly by iRobot.

## Power
While the easiest way to power your virtual wall is via micro-USB with a power outlet, this may not be desirable. There are a few different approaches to operating this device with batteries.

### 9V PP3 battery
Using a 9V battery, you can connect the positive wire to your Nano's VIN pin, and the negative wire to GND. Unfortunately, this will not last very long. The energy will be depleted in a very short amount of time.

### AA battery cells
A more long-lasting approach is to use six AA battery cells in a holder, and then connect it to your Nano

## References
[MKme](https://github.com/MKme/Roomba)

[misc.ws](https://misc.ws/2014/02/27/diy-virtual-wall-for-roomba)

[Roomba Dual Mode Virtual Wall® Barrier](https://store.irobot.com/default/parts-and-accessories/roomba-accessories/i-series/dual-mode-virtual-wall-barrier/4636429.html)
