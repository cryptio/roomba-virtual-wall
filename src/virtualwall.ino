#include <IRremote.h>

IRsend irsend;

void setup() {
  // Our IR LED operates at 38 KHz
  const int khz = 38;

  /* Enable the IR pin
   * NOTE: Pin 3 is hardcoded in the IRremote library for the Nano. The cathode (negative leg) of the IR LED should be wired to D3 along with a resistor
   */
  irsend.enableIROut(khz);
}

/**
 * The Roomba Virtual Wall repeats a pattern of on and off
 * infrared signals using a 38Khz carrier frequency, with each state lasting 1ms
 */
void loop() {
  const unsigned int pulse_usec = 1000;

  // Send an IR signal for 1000 microseconds
  irsend.mark(pulse_usec);
  // Turn off the pin for 1000 microseconds
  irsend.space(pulse_usec);
}
